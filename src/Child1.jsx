import React, {useContext} from 'react';
import {StateContext} from './stateContext';
import {Compo, Description, StatePanel, Paragraph} from './Styles';

const Child1 = () => {

  // use destucturing to pick what we want from the context object 
  const {applicationState, dispatch} = useContext(StateContext); 

  const incrementB = () => dispatch({type: 'DECREMENT_A'});

  return (
    <Compo color='purple' >
      <Description>
      <Paragraph>
    <p >This even more nested component also has access to the state and dispatch method on the StateContext object (in a real project, you might have a 'UserContext' or 'ThemeContext'... I've heard there's a performance hit involved if you have too many of these context providers wrapping each other in Higher Order Components but I've no idea exactly what counts as 'too many'</p>
    </Paragraph>
    <StatePanel>
    <p>{'{ '}state.a {applicationState.a}</p> 
    <p>state.b: {applicationState.b} {' }'} </p>
    <button onClick={incrementB}>decrement a via context </button> 
    </StatePanel>
    </Description>
    </Compo>
  )
};

export default Child1;

