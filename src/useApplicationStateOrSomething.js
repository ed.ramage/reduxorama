import {useReducer} from 'react';

const INCREMENT_A = 'INCREMENT_A'; 
const INCREMENT_B = 'INCREMENT_B'; 
const DECREMENT_A = 'DECREMENT_A';

const initialState = {
  a: 10,
  b: 2,
};

// just like redux, the state is document that we throw away
// and replace every time there's an action. We never edit the 
// state only make a new one and maybe copy some of it over
const reducer = (state, action) => {
  switch (action.type) {
    case INCREMENT_A: return ({...state, a: state.a + 1})
    case INCREMENT_B: return ({...state, b: state.b + 1})
    case DECREMENT_A: return ({...state, a: state.a - 1})
    default: return {...state}
  }
}

const useApplicatinStateOrSomething = () => {

  const [state, dispatch] = useReducer(reducer, initialState);
  
  return [state, dispatch];

};

export default useApplicatinStateOrSomething;

