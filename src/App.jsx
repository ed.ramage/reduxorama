import React from 'react';
import useApplicationStateOrSomething from './useApplicationStateOrSomething';
import {StateContext} from './stateContext';
import NestedComponent from './NestedComponent';
import {Compo, Description, StatePanel, Paragraph} from './Styles';

const App = () => {
  
  const [applicationState, dispatch] = useApplicationStateOrSomething();
console.log(applicationState)  
  // keeping the redux pattern of actions being plain objects with type property 
  const incrementA = () => {
    dispatch({
      type: 'INCREMENT_A'
    })  
  }

  return (
<StateContext.Provider value={{applicationState, dispatch}}>
  <Compo color='blue'>
   <Description>
     <Paragraph>
     <div>
    <p>this top level component initialises the useReducer hook which returns references to the state object and dispatch functions</p>
    <p>{"we pass these references to the Context.Provider value prop.  Now all components wrapped in the StateContext.Provider tag can plumb themselves into the context by importing useContext and the StateContext destructuring whatever you want from the returned object like: const {applicationState, dispatch }= useContext(StateContext);"} </p>
    </div>
    </Paragraph>
    <StatePanel>
    <p>{"{ "}state.a: {applicationState.a} </p>
    <p>state.b: {applicationState.b} {"} "}</p>
    <button onClick={incrementA}>dispatch INCREMENT_A event via Context</button>
    </StatePanel>
    </Description>
    <NestedComponent />
  </Compo>
</StateContext.Provider>
  )
};

export default App;

