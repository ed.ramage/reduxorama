import React, {useContext} from 'react';
import {StateContext} from './stateContext';
import {Compo, Description, StatePanel, Paragraph} from './Styles';
import Child1 from './Child1'; 

const NestedComponent = () => {
  const { applicationState, dispatch } = useContext(StateContext)
  
  // TODO import the constant instead of writing the string 
  const incrementB = () => dispatch({type: 'INCREMENT_B'})

  return (
    <Compo color='beige' >
    <Description>
      <Paragraph>
    <p >This nested component has access to the state and dispatch method</p>
    </Paragraph>
    <StatePanel>
    <p>{'{ '}state.a {applicationState.a}</p> 
    <p>state.b: {applicationState.b} {' }'}</p>
    <button onClick={incrementB}>Increment b via Context</button> 
    </StatePanel>
    </Description>
    <Child1 />
    </Compo>
  )
};

export default NestedComponent;

