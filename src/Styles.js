import styled from 'styled-components';

export const Compo = styled.div`
  background-color: ${props => props.color};
  padding: 1rem;
`

export const Description = styled.div`
  display: flex;
  flex-direction: row;
`

export const StatePanel = styled.div`
  flex: 1;
  border-left: 1px solid yellow;
  padding-left: 1rem;
  margin-bottom: 1rem;
`

export const Paragraph = styled.div`
  flex: 2;
`